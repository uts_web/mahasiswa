@extends('layout.main')

@section('title', 'Tambah Data')


@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong> TAMBAH DATA MAHASISWA </strong></h3>
                                <div class="pull-right">
                                    <a href="{{ url('data_mhs') }}" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-undo"></i> Back
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form action="{{ url('data_mhs') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama_mahasiswa" class="form-control @error('nama_mahasiswa') is-invalid @enderror" value="{{ old('nama_mahasiswa') }}" autofocus>
                                                @error('nama_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>NIM</label>
                                                <input type="text" name="nim_mahasiswa" class="form-control @error('nim_mahasiswa') is-invalid @enderror" value="{{ old('nim_mahasiswa') }}" autofocus>
                                                @error('nim_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" name="email_mahasiswa" class="form-control @error('email_mahasiswa') is-invalid @enderror" value="{{ old('email_mahasiswa') }}" autofocus>
                                                @error('email_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>No Telepon</label>
                                                <input type="text" name="notelp_mahasiswa" class="form-control @error('notelp_mahasiswa') is-invalid @enderror" value="{{ old('notelp_mahasiswa') }}" autofocus>
                                                @error('notelp_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Prodi</label>
                                                <input type="text" name="prodi_mahasiswa" class="form-control @error('prodi_mahasiswa') is-invalid @enderror" value="{{ old('prodi_mahasiswa') }}" autofocus>
                                                @error('prodi_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Jurusan</label>
                                                <input type="text" name="jurusan_mahasiswa" class="form-control @error('jurusan_mahasiswa') is-invalid @enderror" value="{{ old('jurusan_mahasiswa') }}" autofocus>
                                                @error('jurusan_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Fakultas</label>
                                                <input type="text" name="fakultas_mahasiswa" class="form-control @error('fakultas_mahasiswa') is-invalid @enderror" value="{{ old('fakultas_mahasiswa') }}" autofocus>
                                                @error('fakultas_mahasiswa')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
