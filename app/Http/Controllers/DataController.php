<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    public function data()
    {
        $data = DB::table('data')->get();
        //return $data;
        return view('mahasiswa.datamahasiswa', ['data' => $data]);
    }

    public function add ()
    {
        return view('mahasiswa.tambah');
    }

    public function addprocess(Request $request)
    {
        $request->validate([
            'nama_mahasiswa' => 'required|min:3|max:255',
            'nim_mahasiswa' => 'required|min:10|max:10',
            'kelas_mahasiswa' => 'required|min:2|max:255',
            'prodi_mahasiswa' => 'required|min:10|max:255',
            'fakultas_mahasiswa' => 'required|min:10|max:255',
        ]);

        DB::table('data')->insert([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa
        ]);
        return redirect('data_mhs')->with('status', 'Penambahan Data Mahasiswa Berhasil!');
    }

    public function delete($id)
    {
        DB::table('data')->where('id', $id)->delete();
        return redirect('data_mhs')->with('status', 'Hapus Data Mahasiswa Berhasil!');
    }

    public function edit($id)
    {
        $editdata = DB::table('data')->where('id', $id)->first();
        return view('mahasiswa.update', compact('editdata'));
    }

    public function editprocess(Request $request, $id)
    {
        $request->validate([
            'nama_mahasiswa' => 'required|min:3|max:255',
            'nim_mahasiswa' => 'required|min:10|max:10',
            'kelas_mahasiswa' => 'required|min:1|max:255',
            'prodi_mahasiswa' => 'required|min:10|max:255',
            'fakultas_mahasiswa' => 'required|min:10|max:255',
        ]);
        
        DB::table('data')->where('id', $id)
            ->update([
            'nama_mahasiswa' => $request->nama_mahasiswa,
            'nim_mahasiswa' => $request->nim_mahasiswa,
            'kelas_mahasiswa' => $request->kelas_mahasiswa,
            'prodi_mahasiswa' => $request->prodi_mahasiswa,
            'fakultas_mahasiswa' => $request->fakultas_mahasiswa
            ]);
        return redirect('data_mhs')->with('status', 'Update Data Mahasiswa Berhasil!');
    }

}
